﻿
#include <iostream>
#include <string>


class Player
{
    private:
        std::string Name;
        int Score;

    public:
        std::string GetName()
        {
            return Name;
        }
        void SetName(std::string _name)
        {
            Name = _name;
        }

        int GetScore()
        {
            return Score;
        }

        void SetScore(int _score)
        {
            Score = _score;
        }
};

void Sort(Player array[], int arrayLength)
{
    Player temp;
    int i = 0;
    for (int j = 1; j < arrayLength; j++) {
        temp = array[j];
        i = j - 1;
        while (i >= 0 && array[i].GetScore() < temp.GetScore()) {
            array[i + 1] = array[i];
            i = i - 1;
            array[i + 1] = temp;
        }
    }
}

void PrintArray(Player array[], int arrayLength)
{
    for (int i = 0; i < arrayLength; i++)
    {
        std::cout << array[i].GetName() << " " << array[i].GetScore() << '\n';
    }
    std::cout << '\n';
}

int main()
{
    int NumberOfPlayers;
    std::string CurrentName;
    int CurrentScore;

    std::cout << "Enter number of players: ";
    std::cin >> NumberOfPlayers;
    std::cout << '\n';

    Player* array = new Player[NumberOfPlayers];

    for (int i = 0; i < NumberOfPlayers; i++)
    {
        std::cout << "Enter player " << i+1 << " name: ";
        std::cin >> CurrentName;
        array[i].SetName(CurrentName);

        std::cout << "Enter " << CurrentName << "'s score: ";
        std::cin >> CurrentScore;
        array[i].SetScore(CurrentScore);
        std::cout << '\n';
    }

    std::cout << "List of Players:" << '\n';
    PrintArray(array, NumberOfPlayers);
    Sort(array, NumberOfPlayers);
    std::cout << "Sorted List of Players:" << '\n';
    PrintArray(array, NumberOfPlayers);

    delete[] array;
}

